# Joymux

## About

Joymux is a joystick multiplexer for Stepmania on Linux. It allows to set up a
single dance pad for use in 2 player mode in the game to make switching between
local profiles faster. It does so by taking control of the joystick device and
then forwarding the events to one of two virtual joystick devices that you can
map to the players.

## Installation

```console
$ cd joymux
$ make && sudo make install
```

You will have to edit the systemd service file to open the correct device file
for forwarding.

```console
$ vim /etc/systemd/system/joymux.service
$ sudo systemctl daemon-reload
$ sudo systemctl start joymux
```

You can find your device in `/dev/input/by-id/`. Make sure to use a device name
ending in `-event-joystick`. (Not just `-joystick`. Those devices are used for
the deprecated joystick API, which is not supported.)

## Usage

Start Stepmania and map the buttons for both virtual devices to the players.
You can switch the active player with the `joymux-toggle` tool. I recommend
setting up a global hotkey for it on your keyboard (requires borderless
fullscreen mode).

## Known Bugs

Only key presses are forwarded to the virtual devices, so controllers using axis
events are not supported, sorry.

If you switch the active device during a key press the now inactive device
never receives the key release event, so it might be possible to produce an
inconsistent state in a connected program. As this is a program for my personal
use, I just don't do that. ;)
