#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

#include <err.h>
#include <stdio.h>
#include <stdlib.h>

#define CTL_PATH	"/var/run/joymux.socket"

static void usage(void);

int
main(int argc, char **argv)
{
	struct sockaddr_un addr;
	int fd;
	char msg;

	msg = 't';

	if (argc > 2) {
		usage();
	} else if (argc == 2) {
		int n = atoi(argv[1]);
		if (n < 0 || n > 1)
			usage();
		msg = '0' + n;
	}

	fd = socket(AF_UNIX, SOCK_DGRAM, 0);
	if (fd == -1)
		err(1, "socket");

	memset(&addr, 0, sizeof(addr));
	addr.sun_family = AF_UNIX;
	snprintf(addr.sun_path, sizeof(addr.sun_path), "%s", CTL_PATH);

	if (sendto(fd, &msg, 1, 0, (struct sockaddr *)&addr, sizeof(addr)) == -1)
		err(1, "sendto");

	return 0;
}


static void
usage(void)
{
	fprintf(stderr, "usage: joymux-toggle [0 | 1]\n");
	exit(1);
}
