/*
 * Copyright (c) 2019 Martin Natano <natano@natano.net>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <linux/input.h>
#include <linux/uinput.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>

#include <err.h>
#include <errno.h>
#include <fcntl.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define CTL_PATH	"/var/run/joymux.socket"

static int open_joystick(const char *);
static int open_ctl_socket(const char *);
static void copy_props(int, int[2]);
static void copy_events_loop(int, int[2], int);
static void usage(void);

static int active_device = 0;

static inline int
bit_set(unsigned char *bits, unsigned int n)
{
	return bits[n / 8] & (1 << (n % 8));
}

int
main(int argc, char **argv)
{
	struct uinput_setup usetup;
	char name[256];
	int in_fd, out_fds[2], ctl_fd;

	if (argc != 2)
		usage();

	in_fd = open_joystick(argv[1]);

	if (ioctl(in_fd, EVIOCGNAME(sizeof(name)), name) == -1)
		err(1, "failed to fetch joystick name");
	fprintf(stderr, "name: %s\n", name);

	ctl_fd = open_ctl_socket(CTL_PATH);

	out_fds[0] = open("/dev/uinput", O_WRONLY | O_NONBLOCK);
	if (out_fds[0] == -1)
		err(1, "open");

	out_fds[1] = open("/dev/uinput", O_WRONLY | O_NONBLOCK);
	if (out_fds[0] == -1)
		err(1, "open");

	memset(&usetup, 0, sizeof(usetup));
	usetup.id.bustype = BUS_VIRTUAL;

	snprintf(usetup.name, sizeof(usetup.name), "joymux 0: %s", name);
	if (ioctl(out_fds[0], UI_DEV_SETUP, &usetup) == -1)
		err(1, "ioctl");

	snprintf(usetup.name, sizeof(usetup.name), "joymux 1: %s", name);
	if (ioctl(out_fds[1], UI_DEV_SETUP, &usetup) == -1)
		err(1, "ioctl");

	copy_props(in_fd, out_fds);

	if (ioctl(out_fds[0], UI_DEV_CREATE) == -1 ||
	    ioctl(out_fds[1], UI_DEV_CREATE) == -1)
		err(1, "failed to setup output devices");

	copy_events_loop(in_fd, out_fds, ctl_fd);

	(void)close(in_fd);
	(void)close(out_fds[0]);
	(void)close(out_fds[1]);
	(void)close(ctl_fd);
	return 0;
}

static int
open_joystick(const char *path)
{
	int fd;

	fd = open(path, O_RDONLY | O_NONBLOCK);
	if (fd == -1)
		err(1, "failed to open joystick");

	if (ioctl(fd, EVIOCGRAB, 1) == -1)
		warn("failed to grab joystick");

	return fd;
}

static int
open_ctl_socket(const char *path)
{
	struct sockaddr_un addr;
	mode_t m;
	int fd;

	fd = socket(AF_UNIX, SOCK_DGRAM, 0);
	if (fd == -1)
		err(1, "socket");

	if (unlink(path) == -1 && errno != ENOENT)
		err(1, "failed to delete stale control socket");

	memset(&addr, 0, sizeof(addr));
	addr.sun_family = AF_UNIX;
	snprintf(addr.sun_path, sizeof(addr.sun_path), "%s", path);

	if (bind(fd, (struct sockaddr *)&addr, sizeof(addr)) == -1)
		err(1, "failed to create control socket");

	m = umask(0);

	if (chmod(path, 0777) == -1)
		warn("failed to update control socket mode");

	(void)umask(m);

	return fd;
}

static void
copy_props(int in_fd, int out_fds[2])
{
	unsigned char bits[(EV_CNT + 7) / 8];
	unsigned char key_bits[(KEY_CNT + 7) / 8];
	unsigned int key;

	if (ioctl(in_fd, EVIOCGBIT(0, sizeof(bits)), bits) == -1)
		err(1, "failed to fetch supported event types");

	if (!bit_set(bits, EV_KEY))
		errx(1, "joystick doesn't support key events");

	if (ioctl(in_fd, EVIOCGBIT(EV_KEY, sizeof(key_bits)), key_bits) == -1)
		err(1, "failed to fetch supported key events");

	if (ioctl(out_fds[0], UI_SET_EVBIT, EV_KEY) == -1 ||
	    ioctl(out_fds[1], UI_SET_EVBIT, EV_KEY) == -1)
		err(1, "failed to set event bit");

	for (key = 0; key < KEY_CNT; key++) {
		if (bit_set(key_bits, key)) {
			if (ioctl(out_fds[0], UI_SET_KEYBIT, key) == -1 ||
			    ioctl(out_fds[1], UI_SET_KEYBIT, key) == -1)
				err(1, "failed to set key bit");
		}
	}
}

static void
copy_events_loop(int in_fd, int out_fds[2], int ctl_fd)
{
	struct pollfd pfds[2];
	struct input_event ev;
	ssize_t n;
	char c;

	pfds[0].fd = in_fd;
	pfds[0].events = POLLIN;

	pfds[1].fd = ctl_fd;
	pfds[1].events = POLLIN;

	for (;;) {
		if (poll(pfds, 2, -1) == -1) {
			if (errno == EINTR)
				continue;
			err(1, "poll");
		}

		if ((pfds[0].revents | pfds[1].revents) & POLLHUP)
			errx(1, "hangup");

		if (pfds[0].revents & POLLIN) {
			n = read(in_fd, &ev, sizeof(ev));
			if (n == -1)
				err(1, "read");
			else if (n != sizeof(ev))
				errx(1, "short read");

			n = write(out_fds[active_device], &ev, sizeof(ev));
			if (n == -1)
				err(1, "write");
			else if (n != sizeof(ev))
				errx(1, "short write");
		}

		if (pfds[1].revents & POLLIN) {
			n = read(ctl_fd, &c, 1);
			if (n == -1)
				err(1, "read");
			else if (n != 1)
				continue;

			switch (c) {
			case '0':
				active_device = 0;
				fprintf(stderr, "joymux 0 active\n");
				break;
			case '1':
				active_device = 1;
				fprintf(stderr, "joymux 1 active\n");
				break;
			case 't':
				active_device = active_device ? 0 : 1;
				fprintf(stderr, "joymux %d active\n", active_device);
				break;
			default:
				/* ignore */
				break;
			}
		}
	}
}

static void
usage(void)
{
	fprintf(stderr, "usage: joymux DEVICEPATH\n");
	exit(1);
}
