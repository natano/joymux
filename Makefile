.PHONY: all
.SUFFIXES: .c .o

CC= cc

CFLAGS= -std=c99 -D_GNU_SOURCE -O2
CFLAGS+= -Wall -Wextra -Wformat=2
CFLAGS+= -Wmissing-prototypes -Wstrict-prototypes -Wmissing-declarations
CFLAGS+= -Wwrite-strings -Wshadow -Wpointer-arith -Wsign-compare
CFLAGS+= -Wundef -Wbad-function-cast -Winline -Wcast-align

SRCS= joymux.c

all: joymux joymux-toggle

.c.o:

joymux: joymux.c
	$(CC) $(CFLAGS) -o $@ $^

joymux-toggle: joymux-toggle.c
	$(CC) $(CFLAGS) -o $@ $^

clean:
	rm -f joymux joymux-toggle

install:
	install -o root -g root -m 0755 joymux /usr/local/bin/
	install -o root -g root -m 0755 joymux-toggle /usr/local/bin/
	install -o root -g root -m 0644 joymux.service /etc/systemd/system/
